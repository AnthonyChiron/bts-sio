﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fr.gouv.education.ac_nantes.dec.Base;

namespace IDPEC
{
    public class Request
    {
        #region Var
        private char separator = '|';

        /// <summary>
        /// Chemin du fichier des class sauvegardées
        /// </summary>
        public static string requestFilePath = @"E:\IDPEC\IDPEC\bin\Debug\requestPath.txt";

        /// <summary>
        /// Id de la requête
        /// </summary>
        private int id = -1;

        /// <summary>
        /// Nom de la requête
        /// </summary>
        private string name;

        /// <summary>
        /// Description de la requête
        /// </summary>
        private string description;

        /// <summary>
        /// Contenu de la requête
        /// </summary>
        private string content;

        /// <summary>
        /// Liaison de la requête
        /// </summary>
        private eLiaison liaison = eLiaison.DotNet;

        /// <summary>
        /// Serveur de la requête
        /// </summary>
        private eServeur server;

        /// <summary>
        /// Base de la requête
        /// </summary>
        private eBase _base;

        /// <summary>
        /// Domaine de la requête
        /// </summary>
        private eDomaine domaine;

        #endregion

        #region Getters and setters
        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public eDomaine Domaine
        {
            get { return domaine; }
            set { domaine = value; }
        }

        public eBase Base
        {
            get { return _base; }
            set { _base = value; }
        }

        public eServeur Server
        {
            get { return server; }
            set { server = value; }
        }

        public eLiaison Liaison
        {
            get { return liaison; }
            set { liaison = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sauvegarde la requête dans le fichier des requêtes
        /// </summary>
        /// <param name="toSave">Requête à sauvegarder</param>
        public void SaveInFile()
        {
            List<Request> requests = LoadFromFile();
            if (this.Id == -1)
            {
                if (requests == null)
                    this.Id = 0;
                else
                    this.Id = requests.Last().Id + 1;
            }
            File.AppendAllText(requestFilePath, this.CreateRequestLign());
        }

        /// <summary>
        /// Modifie la requête dans le fichier des requêtes
        /// </summary>
        /// <param name="toModify">Requête à modifier</param>
        public void ModifyInFile(Request toModify)
        {
            String[] allLines = File.ReadAllLines(requestFilePath);

            for (int i = 0; i != allLines.Count(); i++)
            {
                if (allLines[i].Contains("id=" + this.id.ToString()))
                {
                    allLines[i] = "id=" + this.Id.ToString() + ';' + "name=" + toModify.Name + ';' + "description=" + toModify.Description + ';' + "content=" + toModify.Content + ";\r\n";
                }
            }
            File.WriteAllLines(requestFilePath, allLines);
        }

        /// <summary>
        /// Charge les requêtes dans une liste à partir du fichier des requêtes
        /// </summary>
        /// <returns>Retourne une liste des requêtes. Retourne null si le fichier de requêtes n'existe pas</returns>
        public static List<Request> LoadFromFile()
        {
            List<Request> allRequests = new List<Request>();
            string line;

            if (File.Exists(requestFilePath) == true)
            {
                using (StreamReader requestFile = new StreamReader(requestFilePath))
                {
                    while ((line = requestFile.ReadLine()) != null)
                    {
                        Request request = new Request();

                        string[] lines = line.Split('|');
                        request.Id = Int32.Parse(lines[0].Split('=')[1]);
                        request.Name = lines[1].Split('=')[1];
                        request.Description = lines[2].Split('=')[1];
                        request.Content = lines[3].Split('=')[1];

                        if (lines[4].Split('=')[1] == "OleDB") request.Liaison = eLiaison.OleDB;
                        if (lines[4].Split('=')[1] == "DotNet") request.Liaison = eLiaison.DotNet;

                        if (lines[5].Split('=')[1] == "DB2") request.Server = eServeur.DB2;
                        if (lines[5].Split('=')[1] == "MySQL") request.Server = eServeur.MySQL;

                        if (lines[6].Split('=')[1] == "SIDO") request.Base = eBase.SIDO;
                        if (lines[6].Split('=')[1] == "EDECA16") request.Base = eBase.EDECA16;
                        if (lines[6].Split('=')[1] == "EDECA") request.Base = eBase.EDECA;

                        if (lines[7].Split('=')[1] == "EDECA") request.Domaine = eDomaine.EDECA;
                        if (lines[7].Split('=')[1] == "REF") request.Domaine = eDomaine.REF;
                        if (lines[7].Split('=')[1] == "ATE") request.Domaine = eDomaine.ATE;
                        if (lines[7].Split('=')[1] == "DNB") request.Domaine = eDomaine.DNB;
                        if (lines[7].Split('=')[1] == "EA") request.Domaine = eDomaine.EA;

                        allRequests.Add(request);
                    }
                    if (allRequests.Count == 0)
                        allRequests = null;
                }
            }
            else
                allRequests = null;
            return (allRequests);
        }

        /// <summary>
        /// Trouve et retourne une requête dans le fichier des requêtes
        /// </summary>
        /// <param name="id">Id de la requête à touver</param>
        public static Request FindInFile(int id)
        {
            Request toFind = new Request();
            string line;

            using (StreamReader requestFile = new StreamReader(requestFilePath))
            {

                while ((line = requestFile.ReadLine()) != null)
                {
                    string[] lines = line.Split('|');
                    if (id == Int32.Parse(lines[0].Split('=')[1]))
                    {
                        toFind.Id = Int32.Parse(lines[0].Split('=')[1]);
                        toFind.Name = lines[1].Split('=')[1];
                        toFind.Description = lines[2].Split('=')[1];
                        toFind.Content = lines[3].Split('=')[1];

                        if (lines[4].Split('=')[1] == "OleDB") toFind.Liaison = eLiaison.OleDB;
                        if (lines[4].Split('=')[1] == "DotNet") toFind.Liaison = eLiaison.DotNet;

                        if (lines[5].Split('=')[1] == "DB2") toFind.Server = eServeur.DB2;
                        if (lines[5].Split('=')[1] == "MySQL") toFind.Server = eServeur.MySQL;

                        if (lines[6].Split('=')[1] == "SIDO") toFind.Base = eBase.SIDO;
                        if (lines[6].Split('=')[1] == "EDECA16") toFind.Base = eBase.EDECA16;
                        if (lines[6].Split('=')[1] == "EDECA") toFind.Base = eBase.EDECA;

                        if (lines[7].Split('=')[1] == "EDECA") toFind.Domaine = eDomaine.EDECA;
                        if (lines[7].Split('=')[1] == "REF") toFind.Domaine = eDomaine.REF;
                        if (lines[7].Split('=')[1] == "ATE") toFind.Domaine = eDomaine.ATE;
                        if (lines[7].Split('=')[1] == "DNB") toFind.Domaine = eDomaine.DNB;
                        if (lines[7].Split('=')[1] == "EA") toFind.Domaine = eDomaine.EA;
                    }
                }
                return (toFind);
            }
        }

        /// <summary>
        /// Supprime la requête dans le fichier des requêtes
        /// </summary>
        /// <param name="id">Id de la requête à supprimer</param>
        public static void DeleteInFile(int id)
        {
            List<Request> File = LoadFromFile();

            System.IO.File.Delete(requestFilePath);
            foreach (Request line in File)
            {
                if (id != line.Id)
                {
                    line.SaveInFile();
                }
            }
        }

        private String CreateRequestLign()
        {
            return ("id=" + this.Id.ToString() + separator + "name=" + this.Name + separator + "description=" + this.Description + separator + "content=" + this.Content + separator + "liaison=" +
                this.Liaison.ToString() + separator + "server=" + this.Server.ToString() + separator + "base=" + this.Base.ToString() + separator + "domaine=" + this.Domaine.ToString() + separator + "\r\n");
        }

        public String DiplayReq()
        {
            return (this.id + " - " + this.Name);
        }

        public override String ToString()
        {
            return (this.id + ";" + this.Name + ";" + this.Description + ";" + this.Content + ";");
        }
        #endregion
    }
}
