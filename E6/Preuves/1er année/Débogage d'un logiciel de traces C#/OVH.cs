﻿using System;
using System.IO;
using System.Threading;

namespace fr.gouv.education.ac_nantes.dec.Sms.OVH
{
    public class SMS
    {
        public static string loginSession;
        public static string mdpSession;
        public static string utilisateur;
        public static string password;
        public static string smsAccount;
        public static string numberFrom;
        public static int version;
        public static int type;

        managerService soapi = null;
        String session = "";


        public SMS()
        {
            //SMSv6.getSmsAccount();
            //SMSv6.sendSms();


            soapi = new managerService();
        }
       

        public void Login()
        {
            try
            {
                DateTime date = DateTime.Now;

                Trace.PrintSansTemps("-------------------------------Login------------------------------------------");
                Trace.PrintSansTemps(string.Format("({0})", date));
                Trace.Print("OVH.Login;Début;Initialisation_Login");

                // Login  
                // Password
                // Langue
                // Multisession

                session = soapi.login(loginSession, password, "fr", false);
                Trace.PrintSansTemps("\tLogin = "+ loginSession + "\n");

                Trace.Print(string.Format("OVH.Login;fin;fin Login, session = {0}\n", session));
            }
            catch (Exception erreur)
            {
                Trace.Print(string.Format("ERREUR LogIn, erreur = {0}\n", erreur));
            }
        }

        public int Send(string To, string Message)
        {
            try
            {


                Trace.Print(string.Format("OVH.Send;Début;param={0},{1}\n", To, Message.Replace("\r\n", " ")));

                if (session == "") return 0;

                #region Regles API OVH : telephonySmsSend
                //
                /* telephonySmsSend
                //
                Description : Send a SMS
                Paramètres : 
                    stringsession : the session id
                    stringsmsAccount : the SMS account
                    stringnumberFrom : the number from
                    stringnumberTo : the number to
                    stringmessage : the message
                    intsmsValidity: the maximum time -in minute(s) - before the message is dropped, defaut is 48 hours
                    intsmsClass : the sms class: flash(0), phone display(1), SIM(2), toolkit(3)
                    intsmsDeferred : the time -in minute(s)- to wait before sending the message, default is 0
                    intsmsPriority : the priority of the message (0 to 3), default is 3
                    intsmsCoding : the sms coding : 1 for 7 bit or 2 for unicode, default is 1
                    stringtag : an optional tag
                    booleannoStop : do not display STOP clause in the message, this requires that this is not an advertising message
                Retour : the SMS id
                */
                #endregion


#if NODEBUG
                int result = 1;
                Trace.Print("OVH.Send;Envoi\n");
                Thread.Sleep(1000);
#else
                

                /* France = 
                    - En raison des filtrages des messages de SFR, les envois ne fonctionneront de manière fiable qu'en alphanumérique vers cet opérateur.
                    - Les expéditeurs sont remplacés par un numéro court dans le cas d'un envoi vers l'opérateur Free.
                */
                string numberTo = To; //"+33666999618";
                string message = Message; //"Message de test SOAPI.";
                int smsValidity = (48 * 60);
                int smsClass = 1;
                int smsDeferred = 0;
                int smsPriority = 3;
                int smsCoding = 1;
                string optionnalTag = "";
                bool nonStop = true;

                //int result = soapi.telephonySmsSend(session, smsAccount, numberFrom, numberTo, message, smsValidity, smsClass, smsDeferred, smsPriority, smsCoding, optionnalTag, nonStop);
                int result = soapi.telephonySmsUserSend(loginSession,mdpSession/*"20160705"*/,smsAccount, numberFrom, numberTo, message, smsValidity, smsClass, smsDeferred, smsPriority, smsCoding, optionnalTag, nonStop);
#endif

                Trace.Print(string.Format("OVH.Send;fin;result (sms id) = {0};\n", result));
                Trace.Print("OVH.Send;fin;Fin de l'envoi;\n");

                return result;
            }
            catch (Exception erreur)
            {
                Trace.PrintSansTemps(string.Format("OVH.Send;ERREUR;erreur = {0}\n", erreur.ToString()));
                return -1;
            }
        }

        public void Logout()
        {
            try
            {

                Trace.Print("OVH.Logout;debut;attente de logout;\n");

                soapi.logout(session);

                Trace.Print("OVH.Logout;fin;session terminée;\n");
                Trace.PrintSansTemps("-------------------------------Logout------------------------------------------");
                Trace.PrintSansTemps("\n");
            }
            catch (IOException erreur)
            {
                Trace.PrintSansTemps(string.Format("ERREUR LogOut, erreur = {0}\n", erreur));
            }
        }

        ~SMS()
        {
            try
            {
                //soapi = null;
                //session = "";
            }
            catch (Exception erreur)
            {
                Trace.PrintSansTemps(string.Format("ERREUR ~SMS(), erreur = {0}\n", erreur));
            }
        }
    }
}
