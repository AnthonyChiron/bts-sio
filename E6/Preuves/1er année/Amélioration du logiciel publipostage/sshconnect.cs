﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Renci.SshNet;
using System.IO;

namespace sshConnect
{
    public partial class Form1 : Form
    {
        string repertoireDestination = @"D:\";

        public Form1()
        {
            InitializeComponent();

            repertoireAffiche.Text = repertoireDestination;
        }

        private void liste_Click(object sender, EventArgs e)
        {
            majListe();
        }

        private void majListe()
        {
            string[] liste;

            using (SshClient ssh = new SshClient("commer.in.ac-nantes.fr", "gen", "T2asier!"))
            {
                ssh.Connect();

                SshCommand result = ssh.RunCommand("find /exam/RTEL/?/*2016*/NA_* /exam/RTEL/?/*2016*/XC.RES-EXAM* -type f -printf '%T@;%g;%p\n' | sort -n -r | cut -d';' -f2-3");

                liste = result.Result.Split('\n');

                ssh.Disconnect();
            }

            listBox.DataSource = liste;
            listBox.Refresh();
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selection = listBox.SelectedItem.ToString();

            string[] partie = selection.Split(';');
            string login = partie[0];
            string fichier = partie[1];

            ancienNom.Text = Path.GetFileName(fichier);

            partie = selection.Split('/');
            string examen = partie[4].Substring(0,3);

            if (examen == "BP0") examen = "BP";

            nouveauNom.Text = "NA_" + examen + "_X.txt";
        }

        private void bRenomme_Click(object sender, EventArgs e)
        {
            if (nouveauNom.Text == "") return;

            string selection = listBox.SelectedItem.ToString();

            string[] partie = selection.Split(';');
            string login = partie[0];
            string fichier = partie[1];

            string nouveauFichier = Path.Combine(Path.GetDirectoryName(fichier), nouveauNom.Text).Replace('\\','/');

            using (SshClient ssh = new SshClient("commer.in.ac-nantes.fr", login, "T2asier!"))
            {
                ssh.Connect();

                SshCommand result = ssh.RunCommand("mv " + fichier + " " + nouveauFichier);                

                ssh.Disconnect();
            }

            majListe();
        }

        private void copie_Click(object sender, EventArgs e)
        {
            string selection = listBox.SelectedItem.ToString();

            string[] partie = selection.Split(';');
            string login = partie[0];
            string fichier = partie[1];

            using (SftpClient sftp = new SftpClient("commer.in.ac-nantes.fr", login, "T2asier!"))
            {
                sftp.Connect();

                string fileName = Path.GetFileName(fichier);

                Stream file1 = File.OpenWrite(Path.Combine(repertoireDestination,fileName));

                DateTime dateheure = sftp.GetLastWriteTimeUtc(fichier);

                sftp.DownloadFile(fichier, file1);

                file1.Close();

                File.SetLastWriteTimeUtc(Path.Combine(repertoireDestination, fileName), dateheure);

                //var result = ssh.RunCommand("find /exam/RTEL/?/*2016*/NA_* -type f -printf '%T@;%g;%p\n' | sort -n | cut -d';' -f2-3");
                sftp.Disconnect();
            }
        }

        private void repertoire_Click(object sender, EventArgs e)
        {
            DialogResult resultat = repertoireCopie.ShowDialog();

            if (resultat == DialogResult.OK)
            {
                repertoireDestination = repertoireCopie.SelectedPath;
                repertoireAffiche.Text = repertoireDestination;
                
            }
        }
    }
}
